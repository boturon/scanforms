def splitXy(np_X, np_y, float_allocation):
    """
    Description: Stratified train-test split
    Input      : Numpy array with the (1) images and the (2) labels
    Output     : 4 Numpy arrays:  images and labels for the train and test sets
    """
    from sklearn.model_selection import train_test_split
    return train_test_split(np_X, np_y, stratify=np_y, test_size=float_allocation)

def buildModel(str_type, tupple_shape):
    """
    Description: Return the specified model built
    Input      : (1) model type (NN,DNN,CNN), (2) Tupple of input shape
    Output     : TF model
    """
    from tensorflow import keras
    if str_type == "NN":
        # https://www.tensorflow.org/tutorials/keras/classification
        return keras.Sequential([
            keras.layers.Flatten(input_shape=tupple_shape),
            keras.layers.Dense(128, activation='relu'),
            keras.layers.Dropout(0.2),
            keras.layers.Dense(2)])
    #if str_type == "DNN":
    if str_type == "CNN":
        # https://sanjayasubedi.com.np/deeplearning/keras-example-cnn-with-fashion-mnist-dataset/
        return keras.Sequential([
            keras.layers.Input(shape=(tupple_shape+(1,))),
            keras.layers.Conv2D(filters=32, kernel_size=(3, 3), activation='relu'),
            keras.layers.Conv2D(filters=64, kernel_size=(3, 3), activation='relu'),
            keras.layers.MaxPool2D(pool_size=(2, 2)),
            keras.layers.Dropout(0.25),
            keras.layers.Flatten(),
            keras.layers.Dense(units=128, activation='relu'),
            keras.layers.Dropout(0.2),
            keras.layers.Dense(units=2, activation='softmax')])
    

def trainModel(dict_paths,str_model,np_X,np_y):
    """
    Description: Trains specified model for identifying crossed boxes 
    Input      : (1) dictionary of paths, (2) model type (NN,DNN,CNN), (3) images, (4) labels
    """
    import tensorflow as tf
    from tensorflow import keras
    import numpy as np
    # Inputs
    train_images, test_images, train_labels, test_labels = splitXy(np_X,np_y,0.3)
    train_labels = keras.utils.to_categorical(train_labels, 2)
    test_labels = keras.utils.to_categorical(test_labels, 2)
    # Model
    model = buildModel(str_model, np_X.shape[1:])
    model.summary(); print('\n'*2)
    # Compile
    model.compile(optimizer='adam',
                  loss=keras.losses.CategoricalCrossentropy(from_logits=True),
                  metrics=['accuracy'])
    if str_model=='CNN': 
        train_images, test_images = (np.expand_dims(i, -1) for i in [train_images, test_images])
    # Fit
    model.fit(train_images, train_labels, batch_size=128, epochs=12, validation_split=0.3)
    # Compare how the model performs on the test datase
    test_loss, test_acc = model.evaluate(test_images,  test_labels, verbose=2)
    print('\nTest accuracy:', test_acc, '\n'*2)
    # Saves Model
    model.save(dict_paths['model_{}'.format(str_model)])
    
def modelPredict(dict_paths,str_model, np_data):
    """
    Description: Opens model
    Input      : (1) dictionary of paths, (2) model type (NN,DNN,CNN)
    Output     : Trained tf model
    """
    import tensorflow as tf; from tensorflow import keras; import numpy as np
    if str_model=='CNN':
        np_data = np.expand_dims(np_data, -1)
    model = keras.models.load_model(dict_paths['model_{}'.format(str_model)])
    pred = np.array(tf.argmax(model.predict(np_data),1))
    matrix = np.stack([np.repeat(np.eye(int(len(pred)/6))[i,:],6) for i in range(int(len(pred)/6))])
    return model, pred, ((matrix*pred).sum(axis=1)>4).astype(int)
    
def modelValidate(dict_paths, col_type, np_pred):
    """
    Description: Produces classification report and confusion matrix
    Input      : (1) dictionary of paths, (2) type of prediction, (3) predictions series
    """
    from sklearn.metrics import classification_report, confusion_matrix
    import pandas as pd
    df_true = pd.read_csv(dict_paths['measurements'], index_col=0)
    np_true = df_true.loc[df_true.type==col_type,'value'].values.astype(int)
    print('\nClasification report:\n', classification_report(np_true, np_pred))
    print('\nConfussion matrix:\n',confusion_matrix(np_true, np_pred))