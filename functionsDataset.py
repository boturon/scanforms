def processImage(image_in):
    """
    Descrition: Opens image, converts to greyscale and resizes to square
    Input     : Path of the image
    Output    : Numpy ndarray
    """
    import numpy as np
    from skimage.transform import resize
    # Number of series to add in each side 
    add_n = int((max(image_in.shape)-min(image_in.shape))/2)
    # Axis of missing rows
    add_dim = int(image_in.shape[0]==max(image_in.shape))
    # Values to be added to the thinnest axis
    add_series = np.ones((
        [add_n,max(image_in.shape)][add_dim],\
        [max(image_in.shape),add_n][add_dim]))
    # Image with the series added in the axis in need
    image_temp = np.concatenate((add_series, image_in, add_series), axis=add_dim)
    # Guarantees that the output image is square
    image_out = image_temp[:min(image_temp.shape),:min(image_temp.shape)]
    # Casts a lower resolution
    return resize(image_out, (32,32))

def balanceDataset(np_imgs, np_labels):
    """
    Description: Adds boxes with crosses with noise to increase the dataset
    Input: unbalanced numpy array of images and labels
    Output     : balanced numpy array of (1) images and (2) labels
    """
    import numpy as np
    output = np.empty(((np_labels==1).sum()*2,32,32))
    nbox = {'empty':(np_labels==0).sum(), 'new':(np_labels==1).sum()-(np_labels==0).sum()}
    # Create empty boxes
    temp_whole = [np_imgs[np_labels==0,:,:]]*(nbox['new']//nbox['empty'])
    temp_miss = [np_imgs[np_labels==0,:,:][:(nbox['new']%nbox['empty']),:,:]]
    noise = np.random.normal(0, 0.05, output[np_labels.shape[0]:,:,:].shape)
    # Add to the dataset
    output[:np_labels.shape[0],:,:] = np_imgs
    output[np_labels.shape[0]:,:,:] = np.concatenate(temp_whole+temp_miss,axis=0) + noise
    output[output<0]=0; output[output>1]=1
    # Add labels of new empty boxes
    output_labels = np.concatenate([np_labels]+[np.zeros(nbox['new'],  dtype=np.int8)])
    return output, output_labels

def makeSplit(dict_paths, str_mode='train', dict_predict=None):
    """
    Description: Processing and balancing of the dataset
    Input      : dictionary of paths
    Output     : (1) dataset, (2) names
    """
    import os; import numpy as np; import imageio
    from sklearn.model_selection import train_test_split
    # Create numpy dataset for images and their labels
    if str_mode=='train':
        dataset_names  = sorted([x for x in os.listdir(dict_paths['images']) if x[-4:]==".png"])
    if str_mode=='predict':
        dataset_names  = sorted(dict_predict.keys())
    dataset_images = np.empty((len(dataset_names), 32, 32), dtype=np.float64)
    dataset_labels = np.empty(len(dataset_names), dtype=np.int8)
    # Process images and store inside np array
    for i, file in enumerate(dataset_names):
        if str_mode == "train":
            image = imageio.imread(dict_paths['images']+file, as_gray=True)/255
        if str_mode == "predict":
            image = np.dot(dict_predict[file], [0.2989, 0.5870, 0.1140] )/255
        dataset_images[i,:,:] = processImage(image)
        dataset_labels[i]     = int(file[0]=="c")
    # Corrects unbalances
    if str_mode == 'train':
        return balanceDataset(dataset_images, dataset_labels)
    if str_mode == 'predict': 
        return dataset_images