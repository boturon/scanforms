def createMeasurements(dict_paths, float_margin, str_mode='train'):
    """
    Description: Function creates  measurements table with dimensions
    Input      : (1) dictionary of paths, (2) optative - window margin for pdf selection  
    Ouput      : Table
    """
    import pandas as pd
    dict_dims = {'px_nr':2337, 'px_nc':1653, 'cm_nr':29.75, 'cm_nc':21.1}
    # Open csv with measurements
    df = pd.read_csv(dict_paths['measurements'], index_col=0)
    # Create columns with px instead of columns
    for i, col in enumerate(df.columns[-4:]):
        # px_i = cm_i/cm_A4*px_A4
        cm_i = df.loc[:,col].copy()
        cm_i.loc[df.type!='string'] = cm_i.loc[df.type!='string'] + [-float_margin, float_margin][i>1] # + manual adjustment (increase area)
        cm_A4 = dict_dims[['cm_nr','cm_nc'][i%2==0]]
        px_A4 = dict_dims[['px_nr','px_nc'][i%2==0]]
        df["{}_px".format(col[:-3])] = (cm_i/cm_A4*px_A4).round(0).astype(int)
    # Return measurements
    if str_mode == 'train':  return df
    elif str_mode == 'predict': return df.loc[df.type=='bool']

# https://stackoverflow.com/questions/57657765/read-pdf-as-a-picture
def openPDF(string_path): 
    """ 
    Description: Converts .pdf into list of images
    Input      : Path string of the pdf file
    Output     : List of images
    """
    from pdf2image import convert_from_path
    return convert_from_path(string_path)

def extractLocation(series_row): 
    """ 
    Description: Prepares coordinates for image crop
    Input      : Series of the measurements table
    Output     : Tupple of coordinates
    """
    x_1, y_1, x_2, y_2 = ['tlj_px', 'tli_px', 'brj_px', 'bri_px']
    return tuple(series_row[i] for i in [x_1, y_1, x_2, y_2])

def makeImages(dict_paths, str_mode, str_box='predict'):
    """
    Description: Creates separate images from original pdf for the creation of the dataset of crosses
    Input      : (1) Dictionary with paths of paths, (2) type box, (3) Save y/n
    """
    if str_mode == 'predict': dict_images = {}
    # Opens pdf with boxes crossed
    bulk_images = openPDF(dict_paths['forms_{}'.format(str_box)])
    # Loop for changing the size of the margins around the selected areas in the pdfs
    for window_size in [0.0, 0.05, 0.1, 0.15, 0.2, 0.25]:
        # Opens measurements table with pixel locations adjusted
        measurements = createMeasurements(dict_paths, window_size, str_mode)
        for i, image in enumerate(bulk_images):
            # Get the page number out of the 16 (in case we have more firms per pdf)
            page = i%16 + 1
            table_conditions = (measurements.page==page)&(measurements.box==1)
            # Loop every checkbox
            for _, box in measurements.loc[table_conditions,:].iterrows():
                selection = image.crop(extractLocation(box))
                selection_name = '{}-{}-{}-{}'.format(i, page, box.name, window_size)
                if str_mode == 'train':
                    selection.save("{}{}_{}.png".format(dict_paths['images'],str_box,selection_name))
                elif str_mode == 'predict':
                    dict_images[selection_name] = selection
    if str_mode == 'predict': return dict_images
