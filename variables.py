paths = {
    'data'         :'../data/scanforms/',
    'images'       :'../data/scanforms/images/',
    'forms_cross'  :'../data/scanforms/forms_cross.pdf',
    'forms_empty'  :'../data/scanforms/forms_empty.pdf',
    'forms_predict':'../data/scanforms/forms_predict.pdf',
    'measurements' :'./measurements.csv',
    'model_NN'     :'./modelNN.h5',
    'model_CNN'    :'./modelCNN.h5'
}